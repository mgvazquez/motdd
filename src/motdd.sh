#!/bin/bash
#set -e
#
#   Script 'motdd' (c)2014 - Version 0.1
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#   
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#        Autor: Manuel Andres, Garcia Vazquez
#        eMail: mvazquez@scabb-island.com.ar
#          Web: http://code.scabb-island.com.ar
#   Repository: http://code.scabb-island.com.ar/motdd
#       Issues: http://code.scabb-island.com.ar/motdd/issues
#
#          Use: <USO>
#
#         Note: <NOTA>
# 

#################### VAR / CONST ###################
    # --- Load Script config ---
    if [[ ( ( $# -eq 0 ) || ( $# -gt 0 && "${1}" != *".cfg" ) ) && -f "${0}.cfg" ]]; then . ${0}.cfg;
    elif [[ $# -gt 0 && "${1}" == *".cfg" && -f "${1}" ]]; then . ${1} ;
    else { /usr/bin/clear; echo -e "\033[1m\033[31mWARN\033[0m: Config file not found, default config was loaded."; /bin/sleep 5s; } fi

    # --- Load Library file ---
    LIBRARY_PATH=${LIBRARY_PATH:-/usr/lib}
    [[ -f ${LIBRARY_PATH}/libscabb.so ]] && . ${LIBRARY_PATH}/libscabb.so || { echo -e "\033[1m\033[31mERROR\033[0m: Library not found (${LIBRARY_PATH}/libscabb.so)"; exit 1; }

    SCRIPT_VERSION="0.1"
    LIBRARY_VERSION_REQ="1.3.8"

    MOTDD_BANNER_PATH=${MOTDD_BANNER_PATH:-"/etc/motdd.d"}
    TIME_LAPS=${TIME_LAPS:-"1m"}
####################################################


#################### FUNCTIONS #####################

####################################################


####################### MAIN #######################
    chkPid;
    chkLibraryVersion;
    while :
        do
        echo "" > /etc/motd
        unset banner
        for banner in $(find ${MOTDD_BANNER_PATH}/* -type f -name *.motd | sort); do
            if [ -f ${banner} ]; then
                . ${banner} `dirname ${banner}`>> /etc/motd; _reply=${?};
                [[ "${_reply}" -gt "0" ]] && echo "${_reply}:${banner}" | error || echo "${_reply}:${banner}" | debug
            fi
        done
        sleep ${TIME_LAPS}
    done
####################################################
