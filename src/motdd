#!/bin/bash
#
# CentOS startup script for the MOT Daemon
#
# chkconfig: - 85 15
# description:  motdd Daemon
# processname: motdd
#
#   Script 'motdd' (c)2014 - Version 0.1
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#        Autor: Manuel Andres, Garcia Vazquez
#        eMail: mvazquez@scabb-island.com.ar
#          Web: http://code.scabb-island.com.ar
#   Repository: http://code.scabb-island.com.ar/motdd
#       Issues: http://code.scabb-island.com.ar/motdd/issues
#
# Source function library
. /etc/rc.d/init.d/functions

#################### VAR / CONST ###################
CMD=$1
prog="MOTD Daemon"
prog_bin="/usr/bin/motdd.sh"
prog_cfg="/etc/sysconfig/motdd.cfg"
pidfile="/var/run/motdd.pid"
####################################################

##################### OTHER ########################
[ -f ${prog_cfg} ] && . ${prog_cfg}
LIBRARY_PATH=${LIBRARY_PATH:-/usr/lib}
[[ -f ${LIBRARY_PATH}/libscabb.so ]] && . ${LIBRARY_PATH}/libscabb.so || { echo -e "\033[1m\033[31mERROR\033[0m: Library not found (${LIBRARY_PATH}/libscabb.so)"; exit 1; }
####################################################

#################### FUNCTIONS #####################
    function chkPidFile() {
        local _pidFile="${PID_PATH}/${PID_FILE}.pid"
        if [[ -f ${_pidFile} ]]; then
            local _isRunning=$(ps ax | awk -F' ' '{print $1}' | grep $(cat ${_pidFile}) | wc -l)
            [[ ${_isRunning} -eq 1 ]] && return 0
        fi
        return 1
    }

    function startDaemon(){
        echo -en "Starting ${prog}:"
        if ! chkPidFile; then
            ${prog_bin} ${prog_cfg} &
            echo_success && echo ""
            return 0
        else
            echo_failure && echo ""
            return 1
        fi
    }

    function stopDaemon(){
        echo -en "Stopping ${prog}:"
        if chkPidFile; then
            local _pid="$(cat ${PID_PATH}/${PID_FILE}.pid)"
            kill -9 ${_pid}
            rm -rf ${PID_PATH}/${PID_FILE}.pid
            echo_success && echo ""
            return 0
        else
            echo_failure && echo ""
            return 1
        fi
    }
####################################################

####################### MAIN #######################
    case "$CMD" in
        start)
            startDaemon
            RETVAL=${?}
            exit ${RETVAL}
            ;;

        stop)
            stopDaemon
            RETVAL=${?}
            exit ${RETVAL}
            ;;

        restart)
            stopDaemon
            sleep 1s
            startDaemon
            exit ${RETVAL}
            ;;

        *)
            echo $"Usage: $0 {start|stop|restart}"
            exit 1
    esac
####################################################