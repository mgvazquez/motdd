#!/bin/bash
#
#   Installation Script for 'motdd' (c)2014 - Version 0.1
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#   
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#        Autor: Manuel Andres, Garcia Vazquez
#        eMail: mvazquez@scabb-island.com.ar
#          Web: http://code.scabb-island.com.ar
#   Repository: http://code.scabb-island.com.ar/motdd
#       Issues: http://code.scabb-island.com.ar/motdd/issues
#
#          Use: <USO>
#
#         Note: <NOTA>
# 

#################### VAR / CONST ###################
    # --- Load Library file ---
    LIBRARY_PATH=${LIBRARY_PATH:-/usr/lib}
    [[ -f ${LIBRARY_PATH}/libscabb.so ]] && . ${LIBRARY_PATH}/libscabb.so || { echo -e "\033[1m\033[31mERROR\033[0m: Library not found (${LIBRARY_PATH}/libscabb.so)"; exit 1; }

    SCRIPT_VERSION="0.1"
    LIBRARY_VERSION_REQ="1.3.8"

    [ "`dirname $0`" == "." ] &&  HOME_PATH="" || HOME_PATH="`dirname $0`"

    STDOUT_BOOL=true
    LOG_LEVEL=info
    LOG_FILE="motdd"
    LOG_PATH="/opt/motdd/logs"
    ELEMENTS=( "/etc/monitor.d" "/etc/motdd.d" "/usr/bin/motdd.sh" "/etc/sysconfig/motdd.cfg" "/etc/init.d/motdd" "/etc/logrotate.d/motdd_log")
####################################################

#################### FUNCTIONS #####################
    function remove(){
        local _element=${1}
        echo -e "- Removing \'${elememt}\'..." | info
        /bin/rm -rf ${_element} 2>&1 | warn
    }

    function install(){
        local _target=`dirname ${1}`
        local _element=`basename ${1}`
        echo -e "- Installing \'${_target}/${_element}\'..." | info
        /bin/cp -rf ${HOME_PATH}/src/${_element} ${_target}/ 2>&1 | warn
    }
####################################################

####################### MAIN #######################
    if ! whoUsayUR "root"; then exit 1; fi
    if chkDskItem -e "/etc/init.d/motdd"; then service motdd stop; fi
    echo -e "Installing \"Message og the Day\" (motd) Daemon..." | info
    for elememt in ${ELEMENTS[@]}; do
        if chkDskItem -e "${elememt}" -r -w; then
            [ "${1}" == "-q" ] && { _REPLY="true"; } || { askYN -q "Would you like to overwrite '${elememt}'"; }
            [ "${_REPLY}" == "true" ] && { INSTALL=( "${INSTALL[@]}" "${elememt}" ); REMOVE=( "${REMOVE[@]}" "${elememt}" ); }
        else
            INSTALL=( "${INSTALL[@]}" "${elememt}" )
        fi
    done
    unset elememt; for elememt in ${REMOVE[@]}; do remove "${elememt}"; done
    unset elememt; for elememt in ${INSTALL[@]}; do install "${elememt}"; done
    /sbin/chkconfig --add motdd
    /sbin/chkconfig motdd on
    service motdd start
    echo -e "Now, uncomment the line with \"PrintMotd no\" in your SSH Daemon cfg file (sshd_config)\nand change it to \"PrintMotd yes\", then restart the SSH Daemon" | info
####################################################